# Developer Advocacy

This document is about how you can contribute to devloper advocacy at GitLab. It includes observations and suggestions that will improve developer experience, content and help reach more developer audience.

*Note: This document is highly under WIP.*

1. [Ways to improve content](https://gitlab.com/haimantikamitra/developer-advocacy#ways-to-improve-content)
2. [Documentation suggestion](https://gitlab.com/haimantikamitra/developer-advocacy#documentation-suggestion)
3. [Community initiatives](https://gitlab.com/haimantikamitra/developer-advocacy#community-initiatives)
4. [Improving DX](https://gitlab.com/haimantikamitra/developer-advocacy#improving-dx)

## Ways to improve content

[GitLab YouTube](https://www.youtube.com/@Gitlab/videos) has some great content, and with more engaging content the viewership can be improved.
Some ideas:
- Work on titles that answer a question. For e.g "How to improve security with GitLab?" if we were to create a video related with security in GitLab.
- Have thumbnails designed with a the person in the video. From a quick competitor analysis, we find that [videos with face value](https://youtu.be/OiFBQmmFORc?si=w90svyMObIp76cTK_) has more viewers than those without.
- Similar to [GitLab customer stories](https://www.youtube.com/watch?v=xLA69Qq5cVs&list=PLFGfElNsQthZG5hdIxVaeLIwGSG6Vw4kb) there should also be a developer highlights, where we talk about how we are empowering developers.
- We should also put efforts into storytelling, especially when it comes to releases. The [GitLab release](https://about.gitlab.com/releases/) page has all the necessary information, but a more detailed Changelog page that talks about what has changed, how it will help our users will be helpful.
- More content needs to be added in [GitLab blog](https://about.gitlab.com/blog/) that also helps in SEO. The screenshots below show that while searching about GitLab, the first content that appears is from different sources and not GitLab.
![SEO](SEO1.png)
![SEO](SEO2.png)


## Documentation suggestion
Suggestions on documentation that will improve developer experience and also help us onboard more developers:
- The [GitLab get started guide](https://about.gitlab.com/get-started/) has no mention of how developers can get started. It talks about small businesses, enterprise and building business case, but not about developers.
- In the [product documenation](https://docs.gitlab.com/), for a smoother developer journey, it will be better to have installation guides first, followed by tutorials, use GitLab, extend, administer and subscribe.
- Adding more visual elements, such as images or GIFs to tutorials would help beginners get started with GitLab. For example, in [this tutorial](https://docs.gitlab.com/ee/ci/quick_start/tutorial.html) adding 1-2 screenshots or a GIF under the `Create a project to hold the Docusaurus files` section would readers understand better. Alteratively, adding hyperlinks to existing documenation about creating a project would also be helpful.
- The developer journey in tutorials can be improved for better understanding. For example, in the [Build your application tutorial](https://docs.gitlab.com/ee/tutorials/build_application.html) the first topic should have been `GitLab CI Fundamentals`, followed by `Create and run your first GitLab CI/CD pipeline` and others. The YouTube can be embedded within the tutorials for better viewership as well as understanding.
- In tutorials, we can work towards reducing the time to hello world (TTHW). Taking example of the same `Build your application tutorial` I will start with `Create and run your first GitLab CI/CD pipeline` and then need to come back to the homepage again and again as there is no `next` button to guide me to the next steps.

## Community initiatives
- Creating a open-source program that brings in more contributors to GitLab. This can be something very similar to [GitLab hackathon](https://about.gitlab.com/community/hackathon/). The goal is to have more people build on and with GitLab and also to gather product feedback. 
- Creating a program for student developers will help in GitLab's education initiatives and also create more awarness about GitLab in the student community. This initiative can include benefits such as offering GitLab certification voucher, career coaching, collaborating with universities and more.
- GitLab has amazing [D&I](https://about.gitlab.com/diversity-inclusion-belonging/), we can use this to have events where we are educating and empowering individuals from the lesser represented sections of the society. 
- Gamifying the [GitLab University](https://university.gitlab.com/) experience to bring in more learners. The idea is to host monthly challenges, have digital badges, give out certification vouchers, so that more developers and enterprises can be aware of GitLab offerings.

## Improving DX
- Currently, GitLab does not support the ability to drag and drop files directly within its GUI. Introducing this feature would greatly benefit developers and teams, enhancing their efficiency and overall experience with GitLab's user interface.
- The editing experience on mobile can be improved. The video below shows that I am unable to make changes on GitLab web from the Chrome browser. It is useful to developers for accessing and editing projects on-the-go.

![](UI.mp4)
- Adding a `pencil` icon on the README file would be great for quick fixes and improve developer experience. Currently, to edit README directly from the web GUI, the developer needs to click on the file first and then edit.
![edit icon](edit.png)
